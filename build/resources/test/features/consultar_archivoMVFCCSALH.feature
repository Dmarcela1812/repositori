#language: es

Característica: Un usuario con acceso a AS400 desea realizar la conexion

  Registrarse en AS400 para realizar las diferentes consultas del modulo de contabilidad y conciliacion

 @caso1
  Escenario:Realizar la validacion con los diferentes tipos de consulta, de los archivos de conciliacion
  del programa MVRCONSLD1 en AS400

    Dado el usuario define los parametros de consulta para el programa
      | archivo   | programa   | consecutivo |
      | MVFCCSALH | MVRCONSLD1 | 2           |
    Cuando el usuario ejecuta el calculo de la conciliacion
   Entonces la conciliacion esperada debera ser igual a la conciliacion generada por el programa MVRCONSLD


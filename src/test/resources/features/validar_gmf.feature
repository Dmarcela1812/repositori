#language: es

Característica: Se debe realizar la verificacion cuando el cliente supere los retiros de 65 UVT
  en el mes, el sistema debe generar cobro del 4X1000 por los valores que excedan dichos UVT al mes

  @caso1
  Escenario:Se debe realizar la verificacion cuando el cliente supere los retiros de 65 UVT
  en el mes, el sistema debe generar cobro del 4X1000 por los valores que excedan dichos UVT al mes.

    Dado el usuario define la fecha en que se procesara el cobro del gmf
    Cuando el usuario ejecuta el programa de contabilidad
    Entonces en el archivo del gmf se debe ver el cobro del gmf

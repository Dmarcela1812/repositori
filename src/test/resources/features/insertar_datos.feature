#language: es

Característica: Se debe realizar la validacion de los insert a los archivos del producto monedero

  @caso1
  Escenario:Realizar insert a los archivos de de conciliación,contabilidad y GMF

    Dado el usuario define los movimientos a ingresar al archivo contable
    Cuando el usuario inserta los datos
    Entonces se debe verificar la correcta inserccion de la informacion

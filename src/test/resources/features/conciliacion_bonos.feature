#language: es

Característica: Validar estructura contable del archivo saldos del dia para el proyecto Bonos

  @caso1
  Escenario:Realizar la validacion con los diferentes tipos de consulta, de los archivos de conciliacion
  del programa BMRCONSLD1 en AS400

    Dado el usuario define los parametros de consulta para el programa de conciliacion BMRCONSLD1
      | archivo   | programa   | consecutivo |
      | MVFCCSALH | MVRCONSLD1 | 2           |
    Cuando el usuario ejecuta el calculo de la conciliacion
    Entonces la conciliacion esperada debera ser igual a la conciliacion generada por el programa BMRCONSLD1

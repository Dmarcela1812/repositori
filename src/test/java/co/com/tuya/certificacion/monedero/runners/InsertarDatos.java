package co.com.tuya.certificacion.monedero.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/insertar_datos.feature",
        glue = "co.com.tuya.certificacion.monedero.stepdefinitions",snippets= SnippetType.CAMELCASE)
public class InsertarDatos {
}

package co.com.tuya.certificacion.monedero.stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class InsertarDatosStepDefinitions {

    @Before
    public void antesDelTest() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^el usuario define los movimientos a ingresar al archivo contable$")
    public void elUsuarioDefineLosMovimientosAIngresarAlArchivoContable() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("marcela").describedAs("Insertar datos a los archivos de conciliación");
    }

    @Cuando("^el usuario inserta los datos$")
    public void elUsuarioInsertaLosDatos() {

    }

    @Entonces("^se debe verificar la correcta inserccion de la informacion$")
    public void seDebeVerificarLaCorrectaInserccionDeLaInformacion() {

    }
}

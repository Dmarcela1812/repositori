package co.com.tuya.certificacion.monedero.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/consultar_archivoMVFCCSALH.feature",
        glue = "co.com.tuya.certificacion.monedero.stepdefinitions",snippets= SnippetType.CAMELCASE,
        tags= {"@caso1"})
public class ConsultarArchivoMVFCCSALH {

}

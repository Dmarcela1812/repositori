package co.com.tuya.certificacion.monedero.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.is;

import java.util.List;
import java.util.Map;

import co.com.tuya.certificacion.monedero.interactions.LeerSql;
import co.com.tuya.certificacion.monedero.questions.ValidacionSqlMVFCCSALH;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.actors.Stage;


public class ConsultarArchivomvfccsalhStepDefinitions {


    Stage validarConciliacion;

    @Before
    public void antesDelTest() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^el usuario define los parametros de consulta para el programa$")
    public void elUsuarioDefineLosParametrosDeConsultaParaElPrograma(DataTable datos) {

        OnStage.setTheStage(new OnlineCast());
        theActorCalled("marcela").describedAs("validar conciliacion as400 monedero");
        List<Map<String, String>> data = (datos.asMaps(String.class, String.class));

        for (int i = 0; i <= data.size() - 1; i++) {

            theActorInTheSpotlight().remember("archivo",data.get(i).get("archivo"));
            theActorInTheSpotlight().remember("programa",data.get(i).get("programa"));
            theActorInTheSpotlight().remember("consecutivo",data.get(i).get("consecutivo"));
        }
    }

    @Cuando("^el usuario ejecuta el calculo de la conciliacion$")
    public void elUsuarioEjecutaElCalculoDeLaConciliacion() {

        theActorInTheSpotlight().attemptsTo(LeerSql.enElArchivo());
    }

    @Entonces("^la conciliacion esperada debera ser igual a la conciliacion generada por el programa MVRCONSLD$")
    public void laConciliacionEsperadaDeberaSerIgualALaConciliacionGeneradaPorElProgramaMVRCONSLD() {

        theActorInTheSpotlight().should(seeThat(ValidacionSqlMVFCCSALH.laConsulta(), is(true)));

    }

}



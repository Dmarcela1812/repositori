package co.com.tuya.certificacion.monedero.stepdefinitions;


import co.com.tuya.certificacion.monedero.tasks.CargarLibrerias;
import co.com.tuya.certificacion.monedero.tasks.GenerarCobroGmf;
import co.com.tuya.certificacion.monedero.tasks.IniciarSesionMyextra;
import co.com.tuya.certificacion.myextra.interactions.OpenMyExtra;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ValidrGmfStepDefinitions {
    @Before
    public void antesDelTest() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^el usuario define la fecha en que se procesara el cobro del gmf$")
    public void elUsuarioDefineLaFechaEnQueSeProcesaraElCobroDelGmf() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("marcela").describedAs("validar conciliacion as400 monedero");

        theActorInTheSpotlight().wasAbleTo(OpenMyExtra.withSession("innova3.EDP"));
        theActorInTheSpotlight().attemptsTo(IniciarSesionMyextra.autenticar());
        theActorInTheSpotlight().attemptsTo(CargarLibrerias.enAs400Con());
    }

    @Cuando("^el usuario ejecuta el programa de contabilidad$")
    public void elUsuarioEjecutaElProgramaDeContabilidad() {
        theActorInTheSpotlight().wasAbleTo(GenerarCobroGmf.archivoGmf());

    }

    @Entonces("^en el archivo del gmf se debe ver el cobro del gmf$")
    public void enElArchivoDelGmfSeDebeVerElCobroDelGmf() {

    }

}

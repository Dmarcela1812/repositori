package co.com.tuya.certificacion.monedero.interactions;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.core.time.InternalSystemClock;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class EsperarUnMomento implements Interaction {

	private int var;

	protected EsperarUnMomento(int var) {
		this.var = var;
	}

	public static EsperarUnMomento esperaClase(int var) {
		return instrumented(EsperarUnMomento.class, var);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		new InternalSystemClock().pauseFor(var);

	}

}

package co.com.tuya.certificacion.monedero.tasks;


import co.com.tuya.certificacion.monedero.interactions.EsperarUnMomento;
import co.com.tuya.certificacion.monedero.ui.AS400;
import co.com.tuya.certificacion.myextra.interactions.Enter;
import co.com.tuya.certificacion.myextra.interactions.Hit;
import co.com.tuya.certificacion.myextra.interactions.Text;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import java.util.logging.Logger;


public class CargarLibrerias implements Task {
    Logger logger = Logger.getLogger("com.library.books");

    @Override
    public <T extends Actor> void performAs(T actor) {

        String validacion = Text.of(AS400.MENSAJE_DE_VALIDACION_HECHO);
        if ("Teclee respuesta (si necesario), pulse Intro.".equals(validacion)) {
            actor.attemptsTo(Hit.theKey("<PF12>"));
        }
        actor.attemptsTo(EsperarUnMomento.esperaClase(2000));
        actor.attemptsTo(Enter.theValue("EDTLIBL").into(AS400.COMMAND1), Hit.theKey("<Enter>"));
        actor.attemptsTo(EsperarUnMomento.esperaClase(2000));
        actor.attemptsTo(Hit.theKey("<TAB>"));
        actor.attemptsTo(Enter.theValue("VPINTUYA").into(AS400.LIBRERIA), Hit.theKey("<Enter>"));
        actor.attemptsTo(EsperarUnMomento.esperaClase(2000));
        actor.attemptsTo(Hit.theKey("<PF12>"));
        actor.attemptsTo(EsperarUnMomento.esperaClase(2000));
    }


    public static CargarLibrerias enAs400Con() {
        return Tasks.instrumented(CargarLibrerias.class);
    }

}

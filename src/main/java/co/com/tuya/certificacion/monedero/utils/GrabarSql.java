package co.com.tuya.certificacion.monedero.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GrabarSql {

    private static ResultSet QueryRecordSet = null;

    public static ResultSet RunSql(String SQL, Connection connection) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(SQL);

        QueryRecordSet = preparedStatement.executeQuery();

        return QueryRecordSet;
}}
package co.com.tuya.certificacion.monedero.tasks;

import co.com.tuya.certificacion.monedero.interactions.EsperarUnMomento;
import co.com.tuya.certificacion.monedero.ui.AS400;
import co.com.tuya.certificacion.myextra.interactions.Enter;
import co.com.tuya.certificacion.myextra.interactions.Hit;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IniciarSesionMyextra implements Task{

        public IniciarSesionMyextra() {
        }

        @Override
        public <T extends Actor> void performAs(T actor) {

            //Strin user=System.getProperty("userAS400");
            String username="USUARIO";
            //String password=System.getProperty("passwordAS400");
            String password="CONTRASENA";

            actor.attemptsTo(Enter.theValue(username).into(AS400.USERNAME)
                    ,Enter.theValue(password).into(AS400.PASSWORD)
                    , Hit.theKey("<Enter>"),
            Hit.theKey("<Enter>"));
            actor.attemptsTo(EsperarUnMomento.esperaClase(3000));
        }

        public static IniciarSesionMyextra autenticar() {
            return instrumented(IniciarSesionMyextra.class);
        }
}

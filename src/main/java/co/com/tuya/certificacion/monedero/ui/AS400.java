package co.com.tuya.certificacion.monedero.ui;


import co.com.tuya.certificacion.myextra.objects.TargetMyExtra;

public class AS400 {
	
	private AS400() {}
	
	
	public static final TargetMyExtra USERNAME = TargetMyExtra.the("username").locatedBy(6, 53, 8);
	public static final TargetMyExtra PASSWORD = TargetMyExtra.the("password").locatedBy(7, 53, 10);
	public static final TargetMyExtra COMMAND1 =TargetMyExtra.the("command").locatedBy(20, 7, 20);
	public static final TargetMyExtra COMMAND_SQL =TargetMyExtra.the("SQL").locatedBy(16, 7, 67);
	public static final TargetMyExtra LIBRERIA =TargetMyExtra.the("SQL").locatedBy(8, 13, 10);

	//

	public static final TargetMyExtra MESSAGE = TargetMyExtra.the("loggin message").locatedBy(2, 1, 18);
	public static final TargetMyExtra MENSAJE_DE_VALIDACION_HECHO = TargetMyExtra.the("Campo en el cual dice 'Teclee respuesta (Si necesario), pulse Intro.'").locatedBy(7, 2, 45);
	public static final TargetMyExtra MENSAJE_EJECUCION_PROGRAMA = TargetMyExtra.the("Campo en el cual dice que el programa se ejecuto exitosamente'").locatedBy(8, 36, 22);
}
